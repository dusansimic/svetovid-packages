debprep() {
	jarname="svetovid-lib"
	install -Dm644 "${srcdir}/dist/${jarname}.jar" "${pkgdir}/usr/share/java/${jarname}/${jarname}-${pkgver}.jar"
	ln -sf "/usr/share/java/${jarname}/${jarname}-${pkgver}.jar" "${pkgdir}/usr/share/java/${jarname}/${jarname}.jar"
	install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${jarname}/LICENSE"
	install -Dm644 "${srcdir}/NOTICE" "${pkgdir}/usr/share/licenses/${jarname}/NOTICE"
	cp -r "${basedir}/deb/DEBIAN" "${pkgdir}/DEBIAN"
}
